package com.crazymaker.cloud.disruptor.demo.business;

import com.lmax.disruptor.dsl.Disruptor;
import io.micrometer.core.instrument.MeterRegistry;

public interface AsyncProducer {

    /**
     * @一句话介绍：   高性能的生产数据
     * @author 40岁老架构师 尼恩 @ 公众号 技术自由圈
     * 1. 异步无锁队列
     * 2.   动态扩容
     * 3.   容量监控
     */
    public void publishData(Long data) ; // * 1. 异步无锁队列

    public  void resize()  ; // * 2.   动态扩容
    public long remainingCapacity() ; //* 3.   容量监控
    public void setMeterRegistry(MeterRegistry meterRegistry);

}
