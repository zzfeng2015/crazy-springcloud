package com.crazymaker.cloud.disruptor.demo.controller;

import com.crazymaker.cloud.disruptor.demo.business.AsyncProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publish")
@Slf4j
public class DisruptorDemoController {

    @Autowired
    AsyncProducer asyncProducer;

    //回显服务
    @RequestMapping(value = "/param", method = RequestMethod.GET)
    public String publish(@RequestParam(value = "p1", required = false) String param1
    ) {
        log.info(Thread.currentThread().getName() + "\t" + "publish :" + param1);

        asyncProducer.publishData(Long.valueOf(param1));
        return "publishData  param: " + param1 ;
    }
}
