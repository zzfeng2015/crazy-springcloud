package com.crazymaker.cloud.disruptor.demo.business.impl;

import com.lmax.disruptor.EventFactory;

public class LongEventFactory implements EventFactory {
    @Override 
    public Object newInstance() { 
        return new LongEvent(); 
    } 
} 