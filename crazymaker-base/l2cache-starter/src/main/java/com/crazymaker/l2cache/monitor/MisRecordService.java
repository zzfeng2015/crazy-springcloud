package com.crazymaker.l2cache.monitor;

import com.crazymaker.l2cache.manager.CacheChannel;
import com.crazymaker.l2cache.redis.RedisGenericCache;
import com.crazymaker.springcloud.common.mpsc.ServiceThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;

import static com.crazymaker.springcloud.common.util.StringUtils.genRandomSrc;

public class MisRecordService extends ServiceThread {
    private static final Logger LOG = LoggerFactory.getLogger(MisRecordService.class);
    public static final String MIS_SUMMARY_KEY = "mis";
    private static LinkedBlockingQueue<Long> timeQueue = new LinkedBlockingQueue<>();
    private final CacheChannel cacheChannel;
    private final PrometheusCustomMonitor prometheusCustomMonitor;
    int setValPre;
    public static final int PERIOD = 60 * 1000;//毫秒

    public MisRecordService(CacheChannel cacheChannel, PrometheusCustomMonitor prometheusCustomMonitor) {
        this.cacheChannel = cacheChannel;
        this.prometheusCustomMonitor = prometheusCustomMonitor;
        setValPre = genRandomSrc();
    }

    public void incrementMis() {
//        System.currentTimeMillis();
        timeQueue.add(MillisecondTimer.SINGLETON.now());
//        this.wakeup();
    }

    @Override
    public void run() {
        LOG.debug(this.getServiceName() + " service started");

        while (!this.isStopped()) {
            this.waitForRunning(1000);

            RedisGenericCache redisGenericCache = (RedisGenericCache) cacheChannel.getHolder().getLevel2Cache("summary");
            Long time = null;
            while ((time = timeQueue.poll()) != PERIOD) {
                //业务代码
                redisGenericCache.zadd(MIS_SUMMARY_KEY, time, String.format("%d#%d", setValPre, time));


            }
            if (null != time) {
                redisGenericCache.zremrangeByScore(MIS_SUMMARY_KEY, 0, time - PERIOD);
                Long sum = redisGenericCache.zcard(MIS_SUMMARY_KEY);
                prometheusCustomMonitor.recordMis(sum);
            }


        }

        LOG.debug(this.getServiceName() + " service end");
    }

    @Override
    public String getServiceName() {
        return "L3Cache HitRecord Service";
    }
}

