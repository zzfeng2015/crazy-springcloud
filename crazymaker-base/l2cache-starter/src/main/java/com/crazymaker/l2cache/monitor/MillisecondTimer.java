package com.crazymaker.l2cache.monitor;

/**
 * System.currentTimeMillis()在java中是最常用的获取系统时间的方法，它返回的是1970年1月1日0点到现在经过的毫秒数。
 *
 * 在系统性能优化的过程中，定位问题的过程发现它似乎有较大性能损耗，
 *
 * 极端情况下1ms调用1次，8核CPU消耗点大概在2~5%左右。
 *
 * 调用频率为100ms时，CPU基本在1%左右。
 *
 * 数据说明单单一个System.currentTimeMillis()高频率调用还是有一定CPU消耗的。
 * 对一个毫秒级的接口来说这个性能损耗不算小。
 *
 * 所以在高并发的接口中还是应该尽量避免高频调用。
 *那有什么策略既可以获取系统时间又避免高频率调用呢。
 *
 * 策略一：如果对时间精确度要求不高的话可以使用独立线程缓存时间戳。
 * 策略二：jni使用Linux的clock_gettime()方法。
 *
 */
class MillisecondTimer {
    private long rate = 0;// 频率  
    private volatile long now = 0;// 当前时间  

    private MillisecondTimer(long rate) {
        this.rate = rate;
        this.now = System.currentTimeMillis();
        start();
    }

    private void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(rate);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                now = System.currentTimeMillis();
            }
        }).start();
    }

    public long now() {
        return now;
    }

    public static final MillisecondTimer SINGLETON = new MillisecondTimer(100);

}