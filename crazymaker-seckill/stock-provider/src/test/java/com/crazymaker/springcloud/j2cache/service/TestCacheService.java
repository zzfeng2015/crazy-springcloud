package com.crazymaker.springcloud.j2cache.service;

import com.crazymaker.springcloud.j2cache.bean.TestBean;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class TestCacheService {

	private final AtomicInteger num = new AtomicInteger(0);

	//首先通过 cachemanger 取得缓存的值， 如果没有取到，再来执行这个方法
	//并且还会将方法的返回值缓存起来备用
	@Cacheable(cacheNames="test" ,key = "'a_num'")
	public Integer getNum() {
		return num.incrementAndGet();
	}
	
	@Cacheable(cacheNames="testBean",key = "'a_bean'")
	public TestBean testBean() {
		TestBean bean = new TestBean();
		bean.setNum(num.incrementAndGet());
		return bean;
	}
	
	@CacheEvict(cacheNames={"test","testBean"},key = "'a_num'")
	public void evict() {
		System.out.println("num = " + num.get());
		System.out.println("模拟的 db删除 " );


	}
	
	public void reset() {
		num.set(0);
	}
	
}
