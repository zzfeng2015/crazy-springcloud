package com.crazymaker.cloud.dubbo.demo.provider.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@Slf4j
public class HelloWordController {

    //回显服务
    @RequestMapping(value = "/world", method = RequestMethod.GET)
    public String echo() {
        log.info(Thread.currentThread().getName() + "\t" + "...echo 被调用");
        return "hello world ";
    }
}
